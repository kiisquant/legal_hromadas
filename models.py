# -*- coding: utf-8 -*-

"""
Models for converting encodings and making tables.
"""

from __future__ import annotations
import pandas as pd
from striprtf_alter import rtf_to_text
from typing import Iterator, Dict, Union, List, Tuple, Set
import re
import datetime
import pytz
from setup import logger


Numeric = Union[int, None]


# End of string regex pattern for the input string to be split by:
RE_PATTERN = re.compile(r"^(?=Системный)|"
                        r"(?=фамилия)|"
                        r"(?=имя)|"
                        r"(?=отчество)|"
                        r"(?=[Дд]ата)|"
                        r"(?=^инн)|"
                        r"(?=^адрес)|"
                        r"(?=^все)|"
                        r"(?=^номер)|"
                        r"(?=^вид)|"
                        r"(?=ФИО)|"
                        r"(?=^другая)", re.MULTILINE)

TIMEZONE = pytz.timezone("Europe/Kiev")


def recode(filename: str) -> str:
    """
    Opens input .RTF file and recodes its contents into a string.
    :param filename: Input file to be recoded.
    """
    with open(filename, 'r', encoding="cp1251") as f:
        r = f.read()

    return rtf_to_text(r)


def unpack(input_str: str, pattern: str = RE_PATTERN, head: Numeric = None) -> Iterator[List[Dict[str, str]]]:
    """
    Transforms decoded string into a dataframe.
    :param input_str: Decoded input string.
    :param pattern: End of string regex pattern for the input string to be split by.
    :param head: Yield only first n rows if head is not None else yield everything.
    """
    if head is not None:
        head += 1

    for number, item in enumerate(input_str.split('\n'*5)[:head]):
        data = []
        for row in re.split(pattern, item):
            records = [r.strip().replace('\n', ' ') for r in row.split(" : ")]
            if len(records) == 2:
                if records != ["фамилия", '0']:
                    data.append({records[0]: records[1].replace('\n', ' ')})
            else:
                try:
                    assert not len(records[0])
                except AssertionError:
                    logger.warning(f"{datetime.datetime.now(tz=TIMEZONE)}: Failed to write {records}\n")
        yield data


def get_columns(unpacked: List[List[Dict[str, str]]]) -> List[str]:
    """
    Get column names from the data keys.
    :param unpacked: Data array which raw keys will be extracted from.
    """
    columns = []
    for u in unpacked:
        for dict_ in u:
            if dict_ != {"фамилия": '0'}:
                key = tuple(key for key in dict_)[0]
                if key not in columns:
                    columns.append(key)
    return columns


def tabulate(data: str) -> pd.DataFrame:
    """
    Creates a data frame from unpacked data.
    :param data: Unpacked data to be written down into the data frame.
    """
    unpacked = list(unpack(data))
    columns = get_columns(unpacked)
    result = {column: [] for column in columns}

    for column in columns:
        for row in unpacked:
            if all(map(lambda item: column not in item, row)):
                result[column].append(None)
            else:
                result[column].append(list(filter(lambda item: column in item, row))[0][column])

    return pd.DataFrame(result)
