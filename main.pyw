# -*- coding: utf-8 -*-

"""
Application entry point.

Example:
    python main.pyw
"""
from PyQt5.QtWidgets import (QMessageBox, QMainWindow, QPushButton, QHBoxLayout, QFileDialog, QApplication, QLabel)
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import Qt
import sys
import models


class MainWindow(QMainWindow):
    """
    Main application window with file choosing dialog.
    """
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self) -> None:
        """
        Initializes UI items.
        """
        # Main window title:
        self.setWindowTitle("Расшифровка .rtf для громад")

        # Button that opens source .rtf:
        self.openButton = QPushButton("Расшифровать исходный файл .rtf", self)
        self.openButton.setGeometry(20, 20, 290, 45)
        self.openButton.clicked.connect(self.processInput)

        # Labels and pictures indicating whether the .rtf has already been decoded.
        # Initially they suppose unset state (i.e. source .rtf not chosen):
        self.tableText = QLabel(self)
        self.tablePixmap = QLabel(self)
        pixmap = QPixmap("img/question.png").scaled(40, 40, Qt.KeepAspectRatio)
        self.tablePixmap.setPixmap(pixmap)
        self.tablePixmap.setAlignment(Qt.AlignLeft)
        self.tableText.setText("Исходный файл не выбран\n и/или не расшифрован.")
        self.tablePixmap.setGeometry(20, 80, 40, 40)
        self.tableText.setGeometry(62, 78, 250, 45)
        self.tableText.setAlignment(Qt.AlignJustify)

        # Button that writes decoded .rtf table down into a .xlsx:
        self.writeButton = QPushButton("Записать результат в .xlsx", self)
        self.writeButton.setGeometry(20, 145, 290, 45)
        self.writeButton.clicked.connect(self.writeTable)

        # Main window geometry tweaking:
        self.setFixedSize(325, 225)
        self.setGeometry(650, 300, 325, 225)
        self.show()

    def processInput(self) -> None:
        """
        Tranforms input .rtf file into a Pandas DataFrame (self.table attribute).
        """
        filename = QFileDialog.getOpenFileName(self, "Открыть файл .rtf", filter="Rich Text Format(*.rtf)")[0]
        if filename != '':
            # Initializing OK and ERROR popups:
            msg = QMessageBox()
            error = QMessageBox()
            error.setIcon(QMessageBox.Critical)
            try:
                # Only .rtf files can be processed:
                assert filename.endswith(".rtf")

                # Setting visuals (cursor and labels) to the waiting state - hourglass etc:
                waiting_pixmap = QPixmap("img/waiting.png").scaled(40, 40, Qt.KeepAspectRatio)
                self.tablePixmap.setPixmap(waiting_pixmap)
                self.tableText.setText("Пожалуйста, подождите...")
                QApplication.setOverrideCursor(Qt.WaitCursor)

                # Processing, decoding, and tabulating the relevant data:
                recoded = models.recode(filename)
                self.table = models.tabulate(recoded)

                # Setting visuals back to regular state:
                QApplication.restoreOverrideCursor()

                # Particularly, labels will indicate that there exists a decoded table already:
                ok_pixmap = QPixmap("img/ok.png").scaled(40, 40, Qt.KeepAspectRatio)
                self.tablePixmap.setPixmap(ok_pixmap)
                self.tableText.setText("Исходный файл успешно\n расшифрован.")

                msg.setText("Исходный файл успешно расшифрован.")
                msg.exec_()
            except AssertionError:
                error.setText("Выбранный файл имеет расширение, отличное от .rtf.\nПожалуйста, выберите .rtf-файл.")
                error.exec_()
            except UnicodeDecodeError as e:
                error.setText(f"Возникла ошибка расшифровки:\n{str(e)}")
                error.exec_()

    def writeTable(self) -> None:
        """
        Writes self.table into a chosen file with .xlsx extension.
        """
        msg = QMessageBox()
        error = QMessageBox()
        error.setIcon(QMessageBox.Critical)

        try:
            assert hasattr(self, "table")
            filename = QFileDialog.getSaveFileName(self, "Записать .xlsx-файл", filter="Microsoft Office Excel(*.xlsx)")[0]
            if filename != '':
                if not filename.endswith(".xlsx"):
                    filename = filename + ".xlsx"
                self.table.to_excel(filename, encoding="cp1251", index=False, header=True, engine="xlsxwriter")
                msg.setText(f"Результаты успешно записаны в файл '{filename}'")
                msg.exec_()
        except (AssertionError, AttributeError):
            error.setText("Пожалуйста, сначала раскодируйте исходный файл.")
            error.exec_()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    main = MainWindow()
    sys.exit(app.exec_())
