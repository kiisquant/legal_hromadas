# -*- coding: utf-8 -*-
"""
Application setup.
"""

import logging
logger = logging.getLogger("main_logger")
handler = logging.FileHandler("main.log", mode='a')
logger.addHandler(handler)
logger.setLevel(logging.WARNING)
