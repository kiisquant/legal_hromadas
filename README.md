# README #

This is README for one of the projects among Ukrainian territorial communities.

### Requirements

- Python 3.6+
- Packages from `requirements.txt`

### Installation and setup ###

- Create virtual environment if desired. 
- Install requirements with pip:
```
$ pip install -r requirements.txt
```
- Launch with:
```
$ python main.pyw
```

### Contacts ###

* Anton Kovalov, [antonk9021@gmail.com](mailto:antonk9021@gmail.com)